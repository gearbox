/*
   UIO-IXGBE kernel back-end
   Copyright (C) 2009 Qualcomm Inc. All rights reserved.
   Written by Max Krasnyansky <maxk@qualcommm.com>

   The UIO-IXGBE is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The UIO-IXGBE is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.
*/

#ifndef UIO_IXGBE_H
#define UIO_IXGBE_H

#include <linux/if_ether.h>
#include <linux/types.h>
#include <asm/page.h>

/* Ioctl defines */
#define UIO_IXGBE_BIND       _IOW('E', 200, int)
struct uio_ixgbe_bind_req {
	char      name[20];
};

/* MAC and PHY info */
struct uio_ixgbe_info {
	uint32_t  irq;
	uint64_t  mmio_base;
	uint32_t  mmio_size;

        uint16_t  mac_type;
        uint8_t   mac_addr[ETH_ALEN];

        uint16_t  phy_type;
        uint32_t  phy_addr;
        uint32_t  phy_id;
        uint32_t  phy_revision;

        uint32_t  num_rx_queues;
        uint32_t  num_tx_queues;
        uint32_t  num_rx_addrs;
        uint32_t  mc_filter_type;
};

#define UIO_IXGBE_INFO       _IOW('E', 201, int)
struct uio_ixgbe_info_req {
	char              name[20];
	struct uio_ixgbe_info info;
};

#define UIO_IXGBE_OPEN       _IOW('E', 202, int)
struct uio_ixgbe_open_req {
	uint32_t          uio_dma_devid;
	struct uio_ixgbe_info info;
};

#define UIO_IXGBE_CLOSE      _IOW('E', 203, int)
#define UIO_IXGBE_RESET      _IOW('E', 204, int)

#define UIO_IXGBE_CHECK_LINK _IOW('E', 205, int)
#define UIO_IXGBE_GET_LINK   _IOW('E', 206, int)
#define UIO_IXGBE_SET_LINK   _IOW('E', 207, int)
struct uio_ixgbe_link_req {
	uint16_t  speed;
	uint16_t  duplex;
	uint16_t  flowctl;
        uint16_t  media_type;
        uint32_t  autoneg_advertised;
        uint8_t   autoneg_wait_to_complete;
	uint16_t  flush;  /* Indicates that TX/RX flush is necessary
			   * after link state changed */
};

#define IXGBESETDEBUG   _IOW('E', 220, int)

#endif /* IXGBE_IOCTL_H */

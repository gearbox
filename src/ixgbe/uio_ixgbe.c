/*
   UIO-IXGBE kernel back-end
   Copyright (C) 2009 Qualcomm Inc. All rights reserved.
   Written by Max Krasnyansky <maxk@qualcommm.com>

   The UIO-IXGBE is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The UIO-IXGBE is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.
*/

#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/sysctl.h>
#include <linux/wait.h>
#include <linux/miscdevice.h>
#include <linux/ioport.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <asm/io.h>

#include "uio-dma.h"

#include "ixgbe_common.h"
#include "ixgbe_type.h"

#include "uio_ixgbe.h"

#ifdef DEBUG
#define IXGBE_DBG(args...) printk(KERN_DEBUG "uio-ixgbe: " args)
#else
#define IXGBE_DBG(args...)
#endif

#define IXGBE_INFO(args...) printk(KERN_INFO "uio-ixgbe: " args)
#define IXGBE_ERR(args...)  printk(KERN_ERR  "uio-ixgbe: " args)

#define VERSION "1.0"

char uio_ixgbe_driver_name[]    = "uio-ixgbe";
char uio_ixgbe_driver_string[]  = "UIO-IXGBE kernel backend";
char uio_ixgbe_driver_version[] = VERSION;
char uio_ixgbe_copyright[]      = "Copyright (c) 2009 Qualcomm Inc. Written by Max Krasnyansky <maxk@qualcomm.com>";

static const struct ixgbe_info *uio_ixgbe_info_tbl[] = {
        [board_82598]       = &ixgbe_82598_info,
};

/* uio_ixgbe_pci_tbl - PCI Device ID Table
 *
 * Wildcard entries (PCI_ANY_ID) should come last
 * Last entry must be all 0s
 *
 * { Vendor ID, Device ID, SubVendor ID, SubDevice ID,
 *   Class, Class Mask, private data (not used) }
 */
static struct pci_device_id uio_ixgbe_pci_tbl[] = {
        {PCI_VDEVICE(INTEL, IXGBE_DEV_ID_82598AF_DUAL_PORT),   board_82598 },
        {PCI_VDEVICE(INTEL, IXGBE_DEV_ID_82598AF_SINGLE_PORT), board_82598 },
        {PCI_VDEVICE(INTEL, IXGBE_DEV_ID_82598EB_CX4),         board_82598 },
        {PCI_VDEVICE(INTEL, IXGBE_DEV_ID_82598_CX4_DUAL_PORT), board_82598 },

        /* required last entry */
        {0, }
};

/* Do not export device table. Let users bind explicitly. */
/* MODULE_DEVICE_TABLE(pci, uio_ixgbe_pci_tbl); */

struct uio_ixgbe_udapter {
	struct list_head      list;
	unsigned int          id;
	uint8_t               zapped;
	uint8_t               up;
	uint8_t               msi;

	struct semaphore      sem;
	spinlock_t            lock;
	atomic_t              refcount;

	struct pci_dev       *pdev;
	unsigned long	      iobase;
	unsigned long         iolen;
	struct ixgbe_hw       hw;

	uint16_t              link_speed;
	uint16_t              link_duplex;

	int                   uio_dmaid;

	uint32_t              eicr;
	wait_queue_head_t     read_wait;
};

static LIST_HEAD(dev_list);

static DECLARE_MUTEX(dev_sem);

static int uio_ixgbe_udapter_inuse(struct uio_ixgbe_udapter *ud)
{
	unsigned ref = atomic_read(&ud->refcount);
	if (ref == 1)
		return 0;
	return 1;
}

static void uio_ixgbe_udapter_get(struct uio_ixgbe_udapter *ud)
{
	atomic_inc(&ud->refcount);
}

static void uio_ixgbe_udapter_put(struct uio_ixgbe_udapter *ud)
{
	if (atomic_dec_and_test(&ud->refcount))
		kfree(ud);
}

static struct uio_ixgbe_udapter *uio_ixgbe_udapter_alloc(void)
{
	struct uio_ixgbe_udapter *ud;

	ud = kmalloc(sizeof(*ud), GFP_KERNEL);
	if (!ud)
		return NULL;
	memset(ud, 0, sizeof(*ud));

	atomic_set(&ud->refcount, 1);

	spin_lock_init(&ud->lock);
	init_MUTEX(&ud->sem);
	init_waitqueue_head(&ud->read_wait);
	return ud;
}

static void uio_ixgbe_release_hw_control(struct uio_ixgbe_udapter *ud)
{
	u32 ctrl_ext;

	/* Let firmware take over control of h/w */
	ctrl_ext = IXGBE_READ_REG(&ud->hw, IXGBE_CTRL_EXT);
	IXGBE_WRITE_REG(&ud->hw, IXGBE_CTRL_EXT,
			ctrl_ext & ~IXGBE_CTRL_EXT_DRV_LOAD);
}

static void uio_ixgbe_take_hw_control(struct uio_ixgbe_udapter *ud)
{
	u32 ctrl_ext;

	/* Let firmware know the driver has taken over */
	ctrl_ext = IXGBE_READ_REG(&ud->hw, IXGBE_CTRL_EXT);
	IXGBE_WRITE_REG(&ud->hw, IXGBE_CTRL_EXT,
			ctrl_ext | IXGBE_CTRL_EXT_DRV_LOAD);
}

static void uio_ixgbe_reset(struct uio_ixgbe_udapter *ud);

static const char* linkspeed_str(unsigned int ls)
{
	switch (ls) {
	case IXGBE_PCI_LINK_SPEED_5000: return "5.0Gb/s";
	case IXGBE_PCI_LINK_SPEED_2500: return "2.5Gb/s";
	}

	return "Unknown";
}

static const char* linkwidth_str(unsigned int lw)
{
	switch (lw) {
        case IXGBE_PCI_LINK_WIDTH_8: return "Width x8";
        case IXGBE_PCI_LINK_WIDTH_4: return "Width x4";
        case IXGBE_PCI_LINK_WIDTH_2: return "Width x2";
        case IXGBE_PCI_LINK_WIDTH_1: return "Width x1";
	}
	return "Unknown";
}

static int __devinit uio_ixgbe_setup(struct uio_ixgbe_udapter *ud, const struct pci_device_id *ent)
{
	const struct ixgbe_info *ii = uio_ixgbe_info_tbl[ent->driver_data];
	struct ixgbe_hw *hw = &ud->hw;
	struct pci_dev *pdev = ud->pdev;
	int err;
        u16 link_status, link_speed, link_width;
        u32 part_num;

	/* PCI config space info */
	hw->vendor_id = pdev->vendor;
	hw->device_id = pdev->device;
	hw->revision_id = pdev->revision;
	hw->subsystem_vendor_id = pdev->subsystem_vendor;
	hw->subsystem_device_id = pdev->subsystem_device;

	/* Setup hw api */
        memcpy(&hw->mac.ops, ii->mac_ops, sizeof(hw->mac.ops));
        hw->mac.type  = ii->mac;

        err = ii->get_invariants(hw);
        if (err)
                return -EIO;

        /* default flow control settings */
        hw->fc.original_type = ixgbe_fc_full;
        hw->fc.type          = ixgbe_fc_full;

        /* select 10G link by default */
        hw->mac.link_mode_select = IXGBE_AUTOC_LMS_10G_LINK_NO_AN;
        if (hw->mac.ops.reset(hw)) {
                dev_err(&pdev->dev, "HW Init failed\n");
                return -EIO;
        }

        if (hw->mac.ops.setup_link_speed(hw, IXGBE_LINK_SPEED_10GB_FULL, true, false)) {
                dev_err(&pdev->dev, "Link Speed setup failed\n");
                return -EIO;
        }

        /* initialize eeprom parameters */
        if (ixgbe_init_eeprom(hw)) {
                dev_err(&pdev->dev, "EEPROM initialization failed\n");
                return -EIO;
        }

        /* make sure the EEPROM is good */
        if (ixgbe_validate_eeprom_checksum(hw, NULL) < 0) {
                dev_err(&pdev->dev, "The EEPROM Checksum Is Not Valid\n");
                return -EIO;
        }

        /* initialize default flow control settings */
        hw->fc.original_type = ixgbe_fc_full;
        hw->fc.type          = ixgbe_fc_full;
        hw->fc.high_water = IXGBE_DEFAULT_FCRTH;
        hw->fc.low_water  = IXGBE_DEFAULT_FCRTL;
        hw->fc.pause_time = IXGBE_DEFAULT_FCPAUSE;

        /* print bus type/speed/width info */
        pci_read_config_word(pdev, IXGBE_PCI_LINK_STATUS, &link_status);
        link_speed = link_status & IXGBE_PCI_LINK_SPEED;
        link_width = link_status & IXGBE_PCI_LINK_WIDTH;

        dev_info(&pdev->dev, "(PCI Express:%s:%s) " "%02x:%02x:%02x:%02x:%02x:%02x\n",
                linkspeed_str(link_speed),
                linkwidth_str(link_width),
		hw->mac.perm_addr[0], hw->mac.perm_addr[1], hw->mac.perm_addr[2],
		hw->mac.perm_addr[3], hw->mac.perm_addr[4], hw->mac.perm_addr[5]);

        ixgbe_read_part_num(hw, &part_num);
        dev_info(&pdev->dev, "MAC: %d, PHY: %d, PBA No: %06x-%03x\n",
                 hw->mac.type, hw->phy.type,
                 (part_num >> 8), (part_num & 0xff));

        if (link_width <= IXGBE_PCI_LINK_WIDTH_4) {
                dev_warn(&pdev->dev, "PCI-Express bandwidth available for "
                         "this card is not sufficient for optimal "
                         "performance.\n");
                dev_warn(&pdev->dev, "For optimal performance a x8 "
                         "PCI-Express slot is required.\n");
        }

	/* reset the hardware with the new settings */
	ixgbe_start_hw(hw);

	uio_ixgbe_take_hw_control(ud);

	return 0;
}

// Allocate new enumerated id.
// Must be called under the device list semaphore
static int uio_ixgbe_alloc_enid(void)
{
	struct uio_ixgbe_udapter *ud;
	unsigned int id = 0;

	list_for_each_entry(ud, &dev_list, list) {
		if (ud->id != id)
			return id;
		id++;
	}

	return id;
}

static int __devinit uio_ixgbe_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
	struct uio_ixgbe_udapter *ud;
	int err;

	IXGBE_DBG("probing device %s\n", pci_name(pdev));

	err = pci_enable_device(pdev);
	if (err)
		return err;

	if (!pci_set_dma_mask(pdev, DMA_64BIT_MASK))
		pci_set_consistent_dma_mask(pdev, DMA_64BIT_MASK);
	else if (!pci_set_dma_mask(pdev, DMA_32BIT_MASK))
		pci_set_consistent_dma_mask(pdev, DMA_32BIT_MASK);
	else {
		IXGBE_ERR("No usable DMA configuration, aborting\n");
		return -EIO;
	}

	err = pci_request_regions(pdev, "uio-uio_ixgbe");
	if (err)
		return err;

	ud = uio_ixgbe_udapter_alloc();
	if (!ud) {
		err = -ENOMEM;
		goto err_alloc;
	}

	pci_set_drvdata(pdev, ud);
	ud->pdev = pdev;

	pci_set_master(pdev);
	pci_save_state(pdev);

	ud->iobase = pci_resource_start(pdev, 0);
	ud->iolen  = pci_resource_len(pdev,   0);

	ud->hw.hw_addr = ioremap(ud->iobase, ud->iolen);
	if (!ud->hw.hw_addr) {
		err = -EIO;
		goto err_ioremap;
	}

	err = uio_ixgbe_setup(ud, ent);
	if (err)
		goto err_setup;

	/* Add to global list */
	down(&dev_sem);
	ud->id = uio_ixgbe_alloc_enid();
	list_add(&ud->list, &dev_list);
	up(&dev_sem);

	IXGBE_INFO("device[%u] %s initialized\n", ud->id, pci_name(pdev));
	IXGBE_INFO("device[%u] %s dma mask %llx\n", ud->id, pci_name(pdev),
		(unsigned long long) pdev->dma_mask);

	return 0;

err_setup:
	iounmap(ud->hw.hw_addr);

err_ioremap:
	kfree(ud);

err_alloc:
	pci_release_regions(pdev);
	return err;
}

static int uio_ixgbe_cmd_close(struct uio_ixgbe_udapter *ud, unsigned long arg);

static void __devexit uio_ixgbe_remove(struct pci_dev *pdev)
{
	struct uio_ixgbe_udapter *ud = pci_get_drvdata(pdev);

	uio_ixgbe_cmd_close(ud, 0);

	uio_ixgbe_release_hw_control(ud);

	iounmap(ud->hw.hw_addr);
	pci_release_regions(pdev);
	pci_disable_device(pdev);

	down(&dev_sem);
	list_del(&ud->list);
	ud->zapped = 1;
	up(&dev_sem);

	IXGBE_INFO("device[%u] %s removed\n", ud->id, pci_name(pdev));

	wake_up_interruptible(&ud->read_wait);
	uio_ixgbe_udapter_put(ud);
}

static int uio_ixgbe_resume(struct pci_dev *pdev)
{
	return 0;
}

static int uio_ixgbe_suspend(struct pci_dev *pdev, pm_message_t state)
{
	return 0;
}

static void uio_ixgbe_shutdown(struct pci_dev *pdev)
{
	uio_ixgbe_suspend(pdev, PMSG_SUSPEND);
}

static pci_ers_result_t uio_ixgbe_io_error_detected(struct pci_dev *pdev, pci_channel_state_t state)
{
	// FIXME: Do something
	return PCI_ERS_RESULT_NEED_RESET;
}

static pci_ers_result_t uio_ixgbe_io_slot_reset(struct pci_dev *pdev)
{
	// FIXME: Do something
	return PCI_ERS_RESULT_RECOVERED;
}

static void uio_ixgbe_io_resume(struct pci_dev *pdev)
{
}

/*
 * Interrupt handler.
 * MSI-X mode is not supported.
 */
static irqreturn_t uio_ixgbe_interrupt(int irq, void *data)
{
	struct uio_ixgbe_udapter *ud = data;
	struct ixgbe_hw *hw = &ud->hw;
	uint32_t eicr;

	eicr = IXGBE_READ_REG(hw, IXGBE_EICR);
	if (unlikely(!eicr))
		return IRQ_NONE;  /* Not our interrupt */

	(void) xchg(&ud->eicr, eicr);

	/* 
	 * We setup EIAM such that interrupts are auto-masked (disabled).
	 * User-space will re-enable them.
	 */
#if 0
	/* We simply disable interrupts here.
	 * User-space app will reanable them. */
	IXGBE_WRITE_REG(hw, IMC, ~0);
	IXGBE_WRITE_FLUSH(hw);
#endif

	wake_up_interruptible(&ud->read_wait);
	return IRQ_HANDLED;
}

static unsigned int uio_ixgbe_poll(struct file *file, poll_table *wait)
{  
	unsigned int mask = POLLOUT | POLLWRNORM;
	struct uio_ixgbe_udapter *ud = file->private_data;
	if (!ud)
		return -EBADFD;

	IXGBE_DBG("poll eicr=0x%x\n", ud->eicr);

	poll_wait(file, &ud->read_wait, wait);
	if (ud->eicr)
		mask |= POLLIN | POLLRDNORM;
 
	return mask;
}

static ssize_t uio_ixgbe_read(struct file * file, char __user * buf, 
			    size_t count, loff_t *pos)
{
	struct uio_ixgbe_udapter *ud = file->private_data;
	DECLARE_WAITQUEUE(wait, current);
	uint32_t eicr;
	int err;

	if (!ud)
		return -EBADFD;

	if (count < sizeof(eicr))
		return -EINVAL;

	add_wait_queue(&ud->read_wait, &wait);
	while (count) {
		set_current_state(TASK_INTERRUPTIBLE);

		if (ud->zapped) {
			err = -ENODEV;
			break;
		}

		eicr = xchg(&ud->eicr, 0);

		IXGBE_DBG("read eicr 0x%x\n", eicr);

		if (!eicr) {
			if (file->f_flags & O_NONBLOCK) {
				err = -EAGAIN;
				break;
			}
			if (signal_pending(current)) {
				err = -ERESTARTSYS;
				break;
			}

			/* Nothing to read, let's sleep */
			schedule();
			continue;
		}
		if (copy_to_user(buf, &eicr, sizeof(eicr)))
			err = -EFAULT;
		else
			err = sizeof(eicr);
		break;
	}

	__set_current_state(TASK_RUNNING);
	remove_wait_queue(&ud->read_wait, &wait);

	return err;
}

static ssize_t uio_ixgbe_write(struct file * file, const char __user * buf,
			     size_t count, loff_t *pos)
{
	return 0;
}

static struct uio_ixgbe_udapter *uio_ixgbe_lookup_enid(char *_id)
{
	struct uio_ixgbe_udapter *ud;
	unsigned int id = simple_strtol(_id, NULL, 10);

	list_for_each_entry(ud, &dev_list, list) {
		if (ud->id == id)
			return ud;
	}

	return NULL;
}

static struct uio_ixgbe_udapter *uio_ixgbe_lookup_pciid(char *id)
{
	struct uio_ixgbe_udapter *ud;

	list_for_each_entry(ud, &dev_list, list) {
		if (!strcmp(pci_name(ud->pdev), id))
			return ud;
	}

	return NULL;
}

static struct uio_ixgbe_udapter *uio_ixgbe_lookup_ud(char *id)
{
	switch (id[0]) {
	case '#':
		return uio_ixgbe_lookup_enid(id + 1);

	default:
		return uio_ixgbe_lookup_pciid(id);
	}
}

static int uio_ixgbe_cmd_bind(struct file *file, void __user *argp)
{
	struct uio_ixgbe_udapter *ud;
	struct uio_ixgbe_bind_req req;
	int err;

	if (copy_from_user(&req, argp, sizeof(req)))
		return -EFAULT;

	IXGBE_DBG("bind req name %s\n", req.name);

	down(&dev_sem);

	ud = uio_ixgbe_lookup_ud(req.name);
	if (!ud) {
		err = -ENOENT;
		goto out;
	}

	// Only one process is alowed to bind
	if (uio_ixgbe_udapter_inuse(ud)) {
		err = -EBUSY;
		goto out;
	}

	uio_ixgbe_udapter_get(ud);
	file->private_data = ud;
	err = 0;

out:
	up(&dev_sem);
	return err;
}

static void uio_ixgbe_populate_info(struct uio_ixgbe_udapter *ud, struct uio_ixgbe_info *info)
{
	info->irq       = ud->pdev->irq;
	info->mmio_base = ud->iobase;
	info->mmio_size = ud->iolen;

	memcpy(info->mac_addr, ud->hw.mac.perm_addr, ETH_ALEN);
        info->mac_type = ud->hw.mac.type;

        info->phy_type = ud->hw.phy.type;
        info->phy_addr = ud->hw.phy.addr;
        info->phy_id   = ud->hw.phy.id;
        info->phy_revision = ud->hw.phy.revision;

        info->num_rx_queues = ud->hw.mac.num_rx_queues;
        info->num_tx_queues = ud->hw.mac.num_tx_queues;
        info->num_rx_addrs  = ud->hw.mac.num_rx_addrs;
        info->mc_filter_type = ud->hw.mac.mc_filter_type;
}

static int uio_ixgbe_cmd_info(struct file *file, void __user *argp)
{
	struct uio_ixgbe_udapter *ud;
	struct uio_ixgbe_info_req req;
	int err;

	if (copy_from_user(&req, argp, sizeof(req)))
		return -EFAULT;

	IXGBE_DBG("info req name %s\n", req.name);

	down(&dev_sem);

	ud = uio_ixgbe_lookup_ud(req.name);
	if (!ud) {
		err = -ENOENT;
		goto out;
	}

	err = 0;
	uio_ixgbe_populate_info(ud, &req.info);

	if (copy_to_user(argp, &req, sizeof(req))) {
		err = -EFAULT;
		goto out;
	}

out:
	up(&dev_sem);
	return err;
}

static inline struct page *__last_page(void *addr, unsigned long size)
{
	return virt_to_page(addr + (PAGE_SIZE << get_order(size)) - 1);
}

static void uio_ixgbe_set_ivar(struct ixgbe_hw *hw, u16 int_alloc_entry, u8 msix_vector)
{
        u32 ivar, index;

        msix_vector |= IXGBE_IVAR_ALLOC_VAL;
        index = (int_alloc_entry >> 2) & 0x1F;
        ivar = IXGBE_READ_REG(hw, IXGBE_IVAR(index));
        ivar &= ~(0xFF << (8 * (int_alloc_entry & 0x3)));
        ivar |= (msix_vector << (8 * (int_alloc_entry & 0x3)));
        IXGBE_WRITE_REG(hw, IXGBE_IVAR(index), ivar);
}

static void uio_ixgbe_def_intr_settings(struct uio_ixgbe_udapter *ud)
{
	struct ixgbe_hw *hw = &ud->hw;
	u32 gpie, mask;

	/* Set default interrupt settings */
        IXGBE_WRITE_REG(hw, IXGBE_EITR(0), 0);

        uio_ixgbe_set_ivar(hw, IXGBE_IVAR_RX_QUEUE(0), 0);
        uio_ixgbe_set_ivar(hw, IXGBE_IVAR_TX_QUEUE(0), 0);

        /* Use EIAM to auto-mask when reading EICR, specifically only
 	 * auto mask tx and rx interrupts */
        IXGBE_WRITE_REG(hw, IXGBE_EIAM, IXGBE_EICS_RTX_QUEUE);

	/* Enable immediate dispatch of the user-defined interrupts */
        gpie = IXGBE_GPIE_EIMEN;
        IXGBE_WRITE_REG(hw, IXGBE_GPIE, gpie);

        /* Clear any pending interrupts */
        IXGBE_READ_REG(hw, IXGBE_EICR);

	/* Enable all interrupts. User space will update this. */
        mask = IXGBE_EIMS_ENABLE_MASK;
        IXGBE_WRITE_REG(hw, IXGBE_EIMS, mask);
        IXGBE_WRITE_FLUSH(hw);
}

static int uio_ixgbe_cmd_open(struct file *file, void __user *argp)
{
	struct uio_ixgbe_udapter *ud = file->private_data;
	struct uio_ixgbe_open_req req;
	int err = 0;
	int irqflags;

	if (ud->up)
		return -EALREADY;

	if (copy_from_user(&req, argp, sizeof(req)))
		return -EFAULT;

	IXGBE_DBG("open req\n");

	err = uio_dma_device_open(&ud->pdev->dev, &ud->uio_dmaid);
	if (err < 0)
		return err;

	/* We do not support MSI-X at this point */
	irqflags = IRQF_SHARED;
	ud->msi = !pci_enable_msi(ud->pdev);
	if (ud->msi)
		irqflags = 0;

	uio_ixgbe_def_intr_settings(ud);

	err = request_irq(ud->pdev->irq, &uio_ixgbe_interrupt, irqflags, pci_name(ud->pdev), ud);
	if (err) {
		IXGBE_ERR("unable to request irq %d\n", ud->pdev->irq);
		goto failed;
	}

	req.uio_dma_devid = ud->uio_dmaid;

	uio_ixgbe_populate_info(ud, &req.info);

	if (copy_to_user(argp, &req, sizeof(req))) {
		err = -EFAULT;
		goto failed;
	}

	ud->up = 1;
	return 0;

failed:
	free_irq(ud->pdev->irq, ud);
	if (ud->msi)
		pci_disable_msi(ud->pdev);

	uio_dma_device_close(ud->uio_dmaid);
	return err;
}

static int uio_ixgbe_cmd_close(struct uio_ixgbe_udapter *ud, unsigned long arg)
{
	struct ixgbe_hw *hw = &ud->hw;

	if (!ud->up)
		return 0;

	uio_dma_device_close(ud->uio_dmaid);

	IXGBE_DBG("close req\n");

	free_irq(ud->pdev->irq, ud);
	if (ud->msi)
		pci_disable_msi(ud->pdev);

	ixgbe_init_hw(hw);

	ud->up = 0;
	return 0;
}

static int uio_ixgbe_cmd_reset(struct uio_ixgbe_udapter *ud, unsigned long arg)
{
	int err = 0;

	if (!ud->up)
		return 0;

	IXGBE_DBG("reset req\n");

	/* We might have to do extra stuff in here */

	uio_ixgbe_reset(ud);

	return err;
}

static int uio_ixgbe_cmd_check_link(struct uio_ixgbe_udapter *ud, void __user *argp)
{
	struct ixgbe_hw *hw = &ud->hw;
	struct uio_ixgbe_link_req req;
	int err = 0, flush = 0;
       	bool link_up;
        u32 link_speed = 0;

	if (!ud->up)
		return -EAGAIN;

	/* Here we do link management that uio_ixgbe_watchdog normally does */

	hw->mac.ops.check_link(hw, &link_speed, &link_up);

	IXGBE_DBG("check_link req speed %u up %u\n", link_speed, link_up);

	if (link_up) {
		if (!ud->link_speed) {
			ud->link_speed  = link_speed;
			ud->link_duplex = 2;

			IXGBE_DBG("link is up %d mbps %s\n", ud->link_speed,
				ud->link_duplex == 2 ?  "Full Duplex" : "Half Duplex");
		}
	} else {
		if (ud->link_speed) {
			ud->link_duplex = 0;
			ud->link_speed  = 0;

			/* We've lost the link, so the controller stops DMA,
			 * but we've got queued Tx work that's never going
			 * to get done. Tell the app to flush TX */
			flush = 1;

			IXGBE_DBG("link is down\n");
		}
	}

	/* Cause software interrupt to ensure rx ring is cleaned */
	IXGBE_WRITE_REG(hw, IXGBE_EICS, IXGBE_EICS_OTHER);

	req.speed   = ud->link_speed;
	req.duplex  = ud->link_duplex;
	req.flush   = flush;

	if (copy_to_user(argp, &req, sizeof(req)))
		err = -EFAULT;

	return err;
}

static int uio_ixgbe_cmd_get_link(struct uio_ixgbe_udapter *ud, void __user *argp)
{
	struct uio_ixgbe_link_req req;
	int err = 0;

	IXGBE_DBG("get_link req\n");

	req.speed       = ud->link_speed;
	req.duplex      = ud->link_duplex;
	req.flush       = 0;
	if (copy_to_user(argp, &req, sizeof(req)))
		err = -EFAULT;

	return err;
}

static int uio_ixgbe_cmd_set_link(struct uio_ixgbe_udapter *ud, void __user *argp)
{
	struct uio_ixgbe_link_req req;

	if (!ud->up)
		return -EAGAIN;

	if (copy_from_user(&req, argp, sizeof(req)))
		return -EFAULT;

	IXGBE_DBG("set_link req\n");

	/* 
	 * Ignore this for now.
	 * Does not make too much sense to mess with link speed on 10Gbe cards.
	 */ 

	return 0;
}

static long uio_ixgbe_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct uio_ixgbe_udapter *ud = file->private_data;
	void __user * argp = (void __user *) arg;
	int err;

	IXGBE_DBG("ioctl cmd=%d arg=%lu ud=%p\n", cmd, arg, ud);

	if (cmd == UIO_IXGBE_INFO)
		return uio_ixgbe_cmd_info(file, argp);

	if (cmd == UIO_IXGBE_BIND) {
		if (ud)
			return -EALREADY;

		return uio_ixgbe_cmd_bind(file, argp);
	}

	if (!ud)
		return -EBADFD;

	down(&ud->sem);

	switch (cmd) {
	case UIO_IXGBE_OPEN:
		err = uio_ixgbe_cmd_open(file, argp);
		break;

	case UIO_IXGBE_CLOSE:
		err = uio_ixgbe_cmd_close(ud, arg);
		break;

	case UIO_IXGBE_RESET:
		err = uio_ixgbe_cmd_reset(ud, arg);
		break;

	case UIO_IXGBE_CHECK_LINK:
		err = uio_ixgbe_cmd_check_link(ud, argp);
		break;

	case UIO_IXGBE_SET_LINK:
		err = uio_ixgbe_cmd_set_link(ud, argp);
		break;

	case UIO_IXGBE_GET_LINK:
		err = uio_ixgbe_cmd_get_link(ud, argp);
		break;

	default:
		err = -EINVAL;
		break;
	};

	up(&ud->sem);

	return err;
}

static int uio_ixgbe_open(struct inode *inode, struct file * file)
{
	file->private_data = NULL;
	return 0;
}

static int uio_ixgbe_close(struct inode *inode, struct file *file)
{
	struct uio_ixgbe_udapter *ud = file->private_data;
	if (!ud)
		return 0;

	IXGBE_DBG("close ud=%p\n", ud);

	down(&ud->sem);
	uio_ixgbe_cmd_close(ud, 0);
	up(&ud->sem);

	uio_ixgbe_udapter_put(ud);
	return 0;
}

static void uio_ixgbe_vm_open(struct vm_area_struct *vma)
{
}

static void uio_ixgbe_vm_close(struct vm_area_struct *vma)
{
}

static int uio_ixgbe_vm_fault(struct vm_area_struct *area, struct vm_fault *fdata)
{
	return VM_FAULT_SIGBUS;
}

static struct vm_operations_struct uio_ixgbe_mmap_ops = {
	.open   = uio_ixgbe_vm_open,
	.close  = uio_ixgbe_vm_close,
	.fault  = uio_ixgbe_vm_fault
};

static int uio_ixgbe_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct uio_ixgbe_udapter *ud = file->private_data;

	unsigned long start = vma->vm_start;
	unsigned long size  = vma->vm_end - vma->vm_start;
	unsigned long pgoff = vma->vm_pgoff;
	unsigned long pfn;

	if (!ud)
		return -ENODEV;

	IXGBE_DBG("mmap ud %p start %lu size %lu pgoff %lu\n", ud, start, size, pgoff);

	if (pgoff)
		return -EINVAL;

	pfn = ud->iobase >> PAGE_SHIFT;

	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	if (remap_pfn_range(vma, start, pfn, size, vma->vm_page_prot))
		return -EAGAIN;

	vma->vm_ops = &uio_ixgbe_mmap_ops;
	return 0;
}

static void uio_ixgbe_reset(struct uio_ixgbe_udapter *ud)
{
	if (ixgbe_init_hw(&ud->hw))
		IXGBE_DBG("hardware error\n");
}

static struct file_operations uio_ixgbe_fops = {
	.owner	 = THIS_MODULE,
	.llseek  = no_llseek,
	.read	 = uio_ixgbe_read,
	.write	 = uio_ixgbe_write,
	.poll	 = uio_ixgbe_poll,
	.open	 = uio_ixgbe_open,
	.release = uio_ixgbe_close,
	.mmap	 = uio_ixgbe_mmap,
	.unlocked_ioctl = uio_ixgbe_ioctl,
};

static struct miscdevice uio_ixgbe_miscdev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "uio-ixgbe",
	.fops = &uio_ixgbe_fops,
};

static struct pci_error_handlers uio_ixgbe_err_handler = {
	.error_detected = uio_ixgbe_io_error_detected,
	.slot_reset     = uio_ixgbe_io_slot_reset,
	.resume         = uio_ixgbe_io_resume,
};

static struct pci_driver uio_ixgbe_driver = {
	.name        = uio_ixgbe_driver_name,
	.id_table    = uio_ixgbe_pci_tbl,
	.probe       = uio_ixgbe_probe,
	.remove      = __devexit_p(uio_ixgbe_remove),
	.shutdown    = uio_ixgbe_shutdown,
	.err_handler = &uio_ixgbe_err_handler,
#ifdef CONFIG_PM
	.suspend     = uio_ixgbe_suspend,
	.resume      = uio_ixgbe_resume,
#endif
};

static int __init uio_ixgbe_init_module(void)
{
	int err;

	printk(KERN_INFO "%s - version %s\n", uio_ixgbe_driver_string, uio_ixgbe_driver_version);
	printk(KERN_INFO "%s\n", uio_ixgbe_copyright);

	err = pci_register_driver(&uio_ixgbe_driver);
	if (!err) {
		err = misc_register(&uio_ixgbe_miscdev);
		if (err) {
			IXGBE_ERR("failed to register misc device\n");
			pci_unregister_driver(&uio_ixgbe_driver);
			return err;
		}
	}

	return err;
}

static void __exit uio_ixgbe_exit_module(void)
{
	misc_deregister(&uio_ixgbe_miscdev);
	pci_unregister_driver(&uio_ixgbe_driver);
}

module_init(uio_ixgbe_init_module);
module_exit(uio_ixgbe_exit_module);

/* ---- */

MODULE_AUTHOR("Maxim Krasnyanskiy <maxk@qualcomm.com>");
MODULE_DESCRIPTION("UIO-IXGBE kernel backend");
MODULE_LICENSE("GPL");
MODULE_VERSION(VERSION);
